package com.example.mytestapp;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.NfcA;
import android.os.Bundle;
import android.os.PowerManager;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.*;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.io.IOException;

public class MyActivity extends Activity implements Runnable {

    public NfcAdapter mNfcAdapter;
    private String[][] mTechLists;
    private PendingIntent pendingIntent;
    private NfcA mTag;

    RelativeLayout ferrisWheelLayout;
    ObjectAnimator spin;
    ObjectAnimator shrink;
    ImageView chairView1, chairView2, chairView3, chairView4;
    ObjectAnimator counterSpin1, counterSpin2, counterSpin3, counterSpin4;
    boolean currentlySpinning;
    protected PowerManager.WakeLock mWakeLock;


    public void onToggleClicked(View view) {
        if (currentlySpinning) {
            stopAnimation(view);
            currentlySpinning = false;
        } else {
            startAnimation(view);
            currentlySpinning = true;
        }
    }

    public void stopAnimation(View view) {
        spin.cancel();
        counterSpin1.cancel();
        counterSpin2.cancel();
        counterSpin3.cancel();
        counterSpin4.cancel();
    }

    public void startAnimation(View view) {
        chairView1 = (ImageView) findViewById(R.id.chair1);
        chairView2 = (ImageView) findViewById(R.id.chair2);
        chairView3 = (ImageView) findViewById(R.id.chair3);
        chairView4 = (ImageView) findViewById(R.id.chair4);
        ferrisWheelLayout = (RelativeLayout) findViewById(R.id.parentWheelLayout);



        spin = ObjectAnimator.ofFloat(ferrisWheelLayout, "rotation", 0f, 360f);
        spin.setRepeatMode(-1);
        spin.setRepeatCount(Animation.INFINITE);
        spin.setDuration(8000);
        spin.setInterpolator(new LinearInterpolator());
        spin.start();
/*
        ResizeWidthAnimation anim = new ResizeWidthAnimation(ferrisWheelLayout, ferrisWheelLayout.getWidth()/2);
        anim.setDuration(2000);
        anim.setRepeatMode(Animation.REVERSE);
        anim.setRepeatCount(Animation.INFINITE);
        anim.setInterpolator(new LinearInterpolator());
        ferrisWheelLayout.startAnimation(anim);
*/

        counterSpin1 = ObjectAnimator
                .ofFloat(chairView1, "rotation", 0f, -360f);
        counterSpin1.setRepeatMode(-1);
        counterSpin1.setRepeatCount(Animation.INFINITE);
        counterSpin1.setDuration(8000);
        counterSpin1.setInterpolator(new LinearInterpolator());
        counterSpin1.start();

        counterSpin2 = ObjectAnimator
                .ofFloat(chairView2, "rotation", 0f, -360f);
        counterSpin2.setRepeatMode(-1);
        counterSpin2.setRepeatCount(Animation.INFINITE);
        counterSpin2.setDuration(8000);
        counterSpin2.setInterpolator(new LinearInterpolator());
        counterSpin2.start();

        counterSpin3 = ObjectAnimator
                .ofFloat(chairView3, "rotation", 0f, -360f);
        counterSpin3.setRepeatMode(-1);
        counterSpin3.setRepeatCount(Animation.INFINITE);
        counterSpin3.setDuration(8000);
        counterSpin3.setInterpolator(new LinearInterpolator());
        counterSpin3.start();

        counterSpin4 = ObjectAnimator
                .ofFloat(chairView4, "rotation", 0f, -360f);
        counterSpin4.setRepeatMode(-1);
        counterSpin4.setRepeatCount(Animation.INFINITE);
        counterSpin4.setDuration(8000);
        counterSpin4.setInterpolator(new LinearInterpolator());
        counterSpin4.start();

    }


    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PowerManager powerManager =
                (PowerManager) getSystemService(Context.POWER_SERVICE);
        mWakeLock =
                powerManager.newWakeLock(PowerManager.FULL_WAKE_LOCK,
                        "Full Wake Lock");
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        // Hide the status bar.
        final int uiOptions =
                View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LOW_PROFILE
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_IMMERSIVE;
        ;
        getWindow().getDecorView().setSystemUiVisibility(uiOptions);
        setContentView(R.layout.main);
        mNfcAdapter = NfcAdapter.getDefaultAdapter(this);
        if (mNfcAdapter != null) {
            //     dialog_text.append("Tap an NFC tag for access\n\r");
            Log.d("mydebug", "Tap an NFC tag for access");
        } else {
            //      dialog_text.append("This phone is not NFC enabled\n\r");
            Log.d("mydebug", "This phone is not NFC enabled");
        }


        // Create the PendingIntent object which will contain the details of the tag that has been scanned
        pendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);

        // Setup a tech list for all desired tag types
        mTechLists = new String[][] { new String[] { NfcA.class.getName() } };


    }




    /**
     * Disable the tag dispatch when the app is no longer in the foreground
     */
    @Override
    public void onPause() {
        super.onPause();
        if (mNfcAdapter != null) mNfcAdapter.disableForegroundDispatch(this);
        mWakeLock.release();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mNfcAdapter != null) mNfcAdapter.enableForegroundDispatch(this, pendingIntent, null, mTechLists);
        mWakeLock.acquire();
    }
    /** A tag has been discovered */
    @Override
    public void onNewIntent(Intent intent){
        Log.d("mydebug", "tag discovered");

            // get the tag object for the discovered tag
            Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);

            // try and get the MifareUltralight instance for this tag
            mTag = NfcA.get(tag);

            // if null then this wasn't a NfcV tag so wait for next time
            if(mTag == null){
                //    dialog_text.append("Not a Nfc V tag\n\r");
                Log.d("mydebug", "not a valid nfc A tag");
            }

            // Start the tag communications thread
            Thread myThread = new Thread(this);
            myThread.start();
        }

    // (we could create other threads for other types of tags)
    public void run(){
        Log.d("mydebug", "NFC dispatching thread is running");
        // try to connect to the Nfc V tag
        try{
            mTag.connect();
            if(mTag.isConnected()){
                Log.d("mydebug", "tag connected");

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {


                        Toast.makeText(MyActivity.this, "blah: " + ferrisWheelLayout.getRotation() + ", number: " + (int)(ferrisWheelLayout.getRotation()+90)/90, Toast.LENGTH_SHORT).show();

                    }
                });
            }
        }catch(IOException e){
            //handle the error here
            Log.e("mydebug", "IO exception beim connect");
        }
   //     setLayoutAnimation2((ViewGroup) findViewById(R.id.rellayout), this);
   /*   runOnUiThread(new Runnable() {
            @Override
            public void run() {
                findViewById(R.id.rellayout).invalidate();
            }
        });       */
    }
    public class ResizeWidthAnimation extends Animation
    {
        private int mWidth;
        private int mStartWidth;
        private View mView;

        public ResizeWidthAnimation(View view, int width)
        {
            mView = view;
            mWidth = width;
            mStartWidth = view.getWidth();
        }

        @Override
        protected void applyTransformation(float interpolatedTime, Transformation t)
        {
            int newWidth = mStartWidth + (int) ((mWidth - mStartWidth) * interpolatedTime);

            mView.getLayoutParams().width = newWidth;
            mView.requestLayout();
        }

        @Override
        public void initialize(int width, int height, int parentWidth, int parentHeight)
        {
            super.initialize(width, height, parentWidth, parentHeight);
        }

        @Override
        public boolean willChangeBounds()
        {
            return true;
        }
    }

    }
